/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.ArrayList;
import java.util.Collection;
import static java.util.Collections.sort;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Black Mage
 */
public class Time {

    private HashMap<String, Jogador> jogadores = new HashMap<>();

    public Time() {
    }

    public HashMap<String, Jogador> getJogadores() {
        return jogadores;
    }

    public void addJogador(String s, Jogador j) {
        jogadores.put(s, j);
    }
    
    public List<Jogador> ordena(JogadorComparator comp){
        List<Jogador> list = new ArrayList(jogadores.values());
        sort(list, comp);
        return list;        
    }
}
