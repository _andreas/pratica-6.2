/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.Comparator;

/**
 *
 * @author Black Mage
 */
public class JogadorComparator implements Comparator<Jogador> {

    private boolean numerica;
    private boolean numCrescente;
    private boolean alfaCrescente;

    public JogadorComparator() {
        this.numerica = true;
        this.numCrescente = true;
        this.alfaCrescente = true;
    }

    public JogadorComparator(boolean numerico, boolean numAsc, boolean alfaAsc) {
        if (numerico) {
            this.numerica = true;
        }
        if (numAsc) {
            this.numCrescente = true;
        }
        if (alfaAsc) {
            this.alfaCrescente = true;
        }
    }

    private int numero;
    private String nome;

    @Override
    public int compare(Jogador o1, Jogador o2) {
        int retorno = 0;
        if (numerica) {
            if (o1.getNumero() == o2.getNumero()) {
                retorno = o1.getNome().compareTo(o2.getNome());
                retorno = (alfaCrescente == true) ? retorno : -retorno;
            }
            else{
                retorno = o1.compareTo(o2);
                retorno = (numCrescente == true) ? retorno : -retorno;
            }
        }
        if(!numerica){
            if(o1.getNome().equals(o2.getNome())){
                retorno = o1.compareTo(o2);
                retorno = (numCrescente == true) ? retorno : -retorno;
            }
            else{
                retorno = o1.getNome().compareTo(o2.getNome());
                retorno = (alfaCrescente == true) ? retorno : -retorno;
            }
        }
        return retorno;
    } 
}
