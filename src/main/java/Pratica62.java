
import static java.util.Collections.binarySearch;
import java.util.Iterator;
import java.util.List;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Black Mage
 */
public class Pratica62 {

    public static void main(String[] args) {

        Time time1 = new Time();
        Time time2 = new Time();

        time1.addJogador("Goleiro", new Jogador(4, "Fulano"));
        time1.addJogador("Lateral", new Jogador(4, "Ciclano"));
        time1.addJogador("Atacante", new Jogador(10, "Beltrano"));
        time1.addJogador("Atacante2", new Jogador(11, "Beltrano"));

        time2.addJogador("Goleiro", new Jogador(1, "João"));
        time2.addJogador("Lateral", new Jogador(7, "José"));
        time2.addJogador("Atacante", new Jogador(15, "Mário"));

        System.out.println("           time 1" + "          time 2");
        for (String posicoes : time1.getJogadores().keySet()) {
            System.out.println(posicoes + " " + time1.getJogadores().get(posicoes) + " / " + time2.getJogadores().get(posicoes));
        }

        List<Jogador> ordena = time1.ordena(new JogadorComparator(true, true, false));
        for (Jogador ordena1 : ordena) {
            System.out.println(ordena1);
        }

        System.out.println(binarySearch(ordena, new Jogador(4, "Fulano"), new JogadorComparator()));

    }

}
